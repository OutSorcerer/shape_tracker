import tensorflow as tf
from tensorflow.python.keras.layers.convolutional import Conv2D
from tensorflow.python.keras.models import Model
from tensorflow.python.keras.engine.base_layer import Layer


def tracked_input_shape_getter(self):
    return self.__tracked_input_shape


def tracked_output_shape_getter(self):
    return tuple(self.compute_output_shape(self.tracked_input_shape))


def get__shape__tracking__init__(shape_tracker):
    def __shape__tracking__init__(self, trainable=True, name=None, dtype=None, dynamic=False, **kwargs):
        tracked_input_shape = None

        if 'input_shape_hint' in kwargs:
            tracked_input_shape = kwargs['input_shape_hint']
            del kwargs['input_shape_hint']

        if 'input_layer_hint' in kwargs:
            if tracked_input_shape is not None:
                raise Exception('Both input_shape_hint and input_layer_hint are specified. Please specify just one of them.')

            input_layer_hint = kwargs['input_layer_hint']
            if input_layer_hint is None:
                raise('Neither input_shape_hint nor input_layer_hint are specified. Please specify one of them.')

            if not hasattr(input_layer_hint, 'tracked_output_shape') or input_layer_hint.tracked_output_shape is None:
                raise Exception('A layer specified as input_shape_hint does not have tracked_output_shape. Was it created inside ShapeTraker context?')

            tracked_input_shape = input_layer_hint.tracked_output_shape
            del kwargs['input_layer_hint']

        if tracked_input_shape is None:
            raise Exception('Cannot determine input shape. Please specify input_shape_hint or input_layer_hint.')

        self.__tracked_input_shape = tracked_input_shape

        shape_tracker.original_layer_init(self, trainable, name, dtype, dynamic, **kwargs)

    return __shape__tracking__init__


class ShapeTraker():
    def __init__(self):
        self.original_layer_init = None

    def __enter__(self):
        self.original_layer_init = Layer.__init__
        Layer.__init__ = get__shape__tracking__init__(self)
        Layer.tracked_input_shape = property(tracked_input_shape_getter)
        Layer.tracked_output_shape = property(tracked_output_shape_getter)
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        Layer.__init__ = self.original_layer_init
        del Layer.tracked_input_shape
        del Layer.tracked_output_shape


class CustomModel(Model):
    def __init__(self) -> None:
        super().__init__()
        with ShapeTraker() as tracker:
            self.conv1 = Conv2D(filters=8, kernel_size=3, padding='valid', input_shape_hint=(None, 100, 100, 3), )
            assert self.conv1.tracked_input_shape == (None, 100, 100, 3)
            assert self.conv1.tracked_output_shape == (None, 98, 98, 8)
            self.conv2 = Conv2D(filters=16, kernel_size=3, padding='valid', input_layer_hint=self.conv1)
            assert self.conv2.tracked_input_shape == (None, 98, 98, 8)
            assert self.conv2.tracked_output_shape == (None, 96, 96, 16)
            print('Asserts passed')

    def call(self, inputs):
        x = inputs
        x = self.conv1(x)
        x = self.conv2(x)
        return x


custom_model = CustomModel()
input = tf.zeros(shape=(1, 100, 100, 3))
output = custom_model(input)
print(f'Runtime output shape is {output.shape}')