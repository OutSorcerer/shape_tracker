The issue that `ShapeTraker` solves is tracking input/output shape of tensors in initialization time TF2. 
[output_shape](https://keras.io/layers/about-keras-layers/), a built-in property from Keras, does not work in this case
with a error like `The layer has never been called and thus has no defined output shape.`  

A quick example:

```python
with ShapeTraker() as tracker:
        self.conv1 = Conv2D(filters=8, kernel_size=3, padding='valid', input_shape_hint=(None, 100, 100, 3), )
        assert self.conv1.tracked_input_shape == (None, 100, 100, 3)
        assert self.conv1.tracked_output_shape == (None, 98, 98, 8)
        self.conv2 = Conv2D(filters=16, kernel_size=3, padding='valid', input_layer_hint=self.conv1)
        assert self.conv2.tracked_input_shape == (None, 98, 98, 8)
        assert self.conv2.tracked_output_shape == (None, 96, 96, 16)
```

* It does not require creating a new subclass with shape-tracking code for every layer,
  it just works for all descendant of base `Layer` class
  
* It patches the constructor of `Layer` class, but only inside `with` block, which ensures that the original constructor
  is used outside `with` block.  
  
* For the first input layer it takes the input shape from the `input_shape_hint` parameter.
  From the second layer you can set it with `input_layer_hint`. 
  It also automatically computes output shape using `compute_output_shape` method.
  
* It temporarily adds two new properties `tracked_input_shape` and `tracked_output_shape`.
  They are only available inside with block, because in runtime you can rely on activation tensor shape.

